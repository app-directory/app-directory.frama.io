# App Directory
An directory with apps for mobile GNU/Linux devices will be created here. It will be based on MGLApps (https://mglapps.frama.io and https://framagit.org/mglapps/mglapps.frama.io) and LINMOBapps (https://linmobapps.frama.io and https://framagit.org/linmobapps/linmobapps.frama.io).

Following technologies will be used:
* [git](https://git-scm.com/) (version control)
* [Hugo](https://gohugo.io/) (static site generator)
* [Foundation](https://get.foundation/) (frontend framework)
* [Netlify CMS](https://www.netlifycms.org/) (in-browser content editing)


## Tasks
* [ ] Branding
    * [ ] Name
    * [ ] Icon
    * [ ] Color
    * [ ] Domain

<br>

* [ ] Content
    * [ ] update entries (can also be done after the migration)
    * [ ] add application id (for the filenames of the entries)
    * [ ] add url to the appstream file
    * [ ] update the license names to use the [SPDX license identifiers](https://spdx.org/licenses)
    * [ ] write script for migrating from the .csv-file to Hugo

<br>

* [ ] Frontend
    * [ ] Create prototypes for the different types of sites / site parts
        * [ ] homepage
        * [ ] header
        * [ ] footer
        * [ ] app entry
        * [ ] app list
        * [ ] other entries
        * [ ] other lists
    * [ ] integrate the site designs into the theme
        * the theme is located at `themes/app-directory`, the layout files are located at `themes/app-directory/layouts`

<br>

* [ ] Structure
    * [ ] create templates for the entries
        * site templates are located at `archetypes`
        * [ ] app
            * [ ] follow the AppStream collection YAML specification (https://www.freedesktop.org/software/appstream/docs/sect-AppStream-YAML.html)
        * [ ] framework
        * [ ] distribution
        * [ ] distribution image
        * [ ] device
        * [ ] (device manufacturer?)

<br>

* [ ] Licensing
    * [ ] properly credit MGLApps, LINMOBapps and their contributors
    * [ ] which license to choose for the theme? (AGPL)
    * [ ] which license to choose for the other things which need a license (e.g. the scripts)? (maybe also AGPL)

<br>

* [ ] Integrate AppStream files
    * [ ] ...

<br>

* [ ] Media
    * [ ] a screenshot service (https://www.freedesktop.org/software/appstream/docs/chap-AppStream-Services.html#sect-AppStream-Services-Screenshots) like https://screenshots.debian.net/about could be used
    * [ ] how to handle licenses of images and other media? (maybe create a separate metadata file for every media file to store the licensing information)
    * [ ] should the media be stored in another place? Otherwise the main repository could become very big
    * [ ] how to handle icons
        https://www.freedesktop.org/software/appstream/docs/sect-AppStream-IconCache.html
        * [ ] how are the icons licensed?
        * [ ] integrating them from the source or serving them from own infrastructure (e.g. an additional repository)?

<br>

* [ ] [Netlify CMS](https://www.netlifycms.org/)
    * [ ] add it to the site (https://www.netlifycms.org/docs/add-to-your-site/)
    * [ ] configure it (https://www.netlifycms.org/docs/add-to-your-site/#configuration)
    * [ ] Authentication
        * [ ] is additional authentication using Git Gateway (https://github.com/netlify/git-gateway) and GoTrue (https://github.com/netlify/gotrue) needed or is the authentication provided by the GitLab instance enough?
        * [ ] configure backend (https://www.netlifycms.org/docs/backends-overview)

<br>

* [ ] Search
    * [ ] further evaluate options (e.g. using https://gohugo.io/tools/search/)
    good options are:
        * https://gist.github.com/eddiewebb/735feb48f50f0ddd65ae5606a1cb41ae
        * https://gist.github.com/cmod/5410eae147e4318164258742dd053993

<br>

* [ ] [Repology](https://repology.org/)
    * [ ] integrate badge
    * [ ] add data to the app entries using the API (https://repology.org/api)

<br>

* [ ] Promotion

## Possible roadmap
### v0.1
* add appid for all entries
* script for data migration
* table view
* readme/about/roadmap

### v0.2
* add url to appdata files for all entries
* script for appdata scraping
* integration of appdata data in the UI

### v0.3
* proper UI

# License

The content (located at `content`) is licensed under CC BY-SA 4.0 International: [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/).

Everything else is licensed under the GNU Affero General Public License v3.0 ([https://www.gnu.org/licenses/agpl-3.0.html](https://www.gnu.org/licenses/agpl-3.0.html))

```
This project (except the content (located at `content`) which is licensed under
CC BY-SA 4.0 International) is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This project (except the content (located at `content`) which is licensed under
CC BY-SA 4.0 International) is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this project.  If not, see <https://www.gnu.org/licenses/>.
```
